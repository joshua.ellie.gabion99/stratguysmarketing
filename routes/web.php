<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing_page');
});

Route::get('/contact', function () {
    return view('contact_page');
});
Route::get('/services', function () {
    return view('service_page');
});

Route::get('/about', function () {
    return view('about_page');
});

Auth::routes();