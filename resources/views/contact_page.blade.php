@extends('layouts.app')
@section('styles')
    <style>
        .contents {
            height: 600px;
        }
        .form-container {
            width: 400px;
            height: 550px;
            margin: auto;
            background-color: #F05D39;
            padding: 5px;
        }
        .text_field {
            margin: 2px 2px;
            width: 350px;
            height: 30px;
            font-size: 1.5em;
        }
        table {
            margin: auto;
        }
    </style>
@endsection
@section('content')
    <div class="contents">
        <table>
            <tr>
                <td>
                    <div class="form-container">
                        <h1>Contact Form</h1>
                        <form action="">
                            <input type="text" class="text_field" name="first_name" id="first_name" placeholder="First Name">
                            <input type="text" class="text_field" name="last_name" id="last_name" placeholder="Last Name">
                            <input type="text" class="text_field" name="company_name" id="company_name" placeholder="Company Name">
                            <input type="text" class="text_field" name="email_address" id="email_address" placeholder="Email Address">
                            <input type="text" class="text_field" name="phone_number" id="phone_number" placeholder="Phone Number">
                            <input type="submit" value="Send">
                        </form>
                    </div>
                </td>
                <td>
                    <div class="form-container">
                        Contact us anytime
                    </div>
                </td>
            </tr>
        </table>
    </div>
@endsection
@section('scripts')
    
@endsection