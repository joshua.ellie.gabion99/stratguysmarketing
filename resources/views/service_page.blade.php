@extends('layouts.app')
@section('styles')
    <style>
        
        .service_container {
            padding: 25px;
            height: 700px;
        }
        
        .socialmedia_container, .webdevelopment_container {
            position: relative;
            margin: 10px 5px;

        }
        .socialmedia_text, .webdevelopment_text {
            background-color: blue;
            height: 250px;
            z-index: 0;
            
        }
        .socialmedia_description, .webdevelopment_description {
            position: absolute;
            top: 0;
            background-color: #F05D39;
            height: 250px;
            z-index: 1;
            transition: .5;
            display: none;
        }
        .webdevelopment_text {
            background-image: url("/images/webdesign.jpg");
        }
        .socialmedia_text {
            background-image: url("/images/socialmediacontentmanager.jpg");
        }
    </style>
@endsection
@section('content')
    <div class="contents">
        <div class="service_container">
            <div class="socialmedia_container">
                <div class="socialmedia_text">
                    Social Media Content Manager
                </div>
                <div class="socialmedia_description">
                    The social content media manager is responsible for management and development of activity on core social media channels with the aim of driving engagement, building audience, increasing web traffic and ultimately driving website registrations
                </div>
            </div>
            <div class="webdevelopment_container">
                <div class="webdevelopment_text">
                    Web Development and Design
                </div>
                <div class="webdevelopment_description">
                    Web design and development is an umbrella term that describes the process of creating a website. Like the name suggests, it involves two major skill sets: web design and web development. Web design determines the look and feel of a website, while web development determines how it functions.
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(".webdevelopment_text").hover(function() {
            $(".webdevelopment_description").fadeIn();
        });
        $(".webdevelopment_description").hover(function() {
        },function() {
            $(".webdevelopment_description").fadeOut();
        });
        $(".socialmedia_text").hover(function() {
            $(".socialmedia_description").fadeIn();
        });
        $(".socialmedia_description").hover(function() {
        },function() {
            $(".socialmedia_description").fadeOut();
        });
    </script>
@endsection