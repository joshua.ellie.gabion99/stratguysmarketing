<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Stratguys Marketing</title>
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

</head>
<style>
    body {
        position: relative;
    }
    .header {
        height: 150px;
        background-color: #203C30;
    }
    footer {
        padding-top: 10px;
        padding-bottom: 10px;
        background-color: #000000;
        padding: 20 10;
        left: 0;
        bottom: 0;
        width: 100%;
        color: white;
        text-align: center;
    }
    .footer_link
    {
        margin: 0 10px;
    }
    .socMediaLinks
    {
        margin: 10px 10px;
    }
    .socMedia
    {
        height: 50px;
        width: 50px;
        margin: 0px 10px;
    }
    .header_text, .footer_text {
        color: white;
        font-size: 2em;
    }
    .tags {
        color: white;
        text-decoration: none;
        cursor: pointer;
        transition: .3s;
    }
    .pagetags {
        float: right;
        font-size: 2em;
    }
    .tags:hover{
        color: gray;
    }
    .companyname {
        color: white;
        text-decoration: none;
        font-size: 2em;
        cursor: pointer;
    }
    .counter_container {
        background-color: blue;
        font-size: 1.5em;
        width: 500px;
        margin: auto;
    }
</style>
@yield("styles")
<body>
    <header class="header">
        <a class="companyname">
            Redesign the way you market
        </a>
        <button>
            Hire us
        </button>
        <div class="pagetags">
            <a href="/" class="tags">Home</a>
            <a href="/contact" class="tags">Contact</a>
            <a href="/services" class="tags">Services</a>
            <a href="/about" class="tags">About Us</a>
        </div>
    </header>
    @yield('content')
    <footer class="footer">
        <div>
            Strat Guys Marketing
        </div>
        <div class="socMediaLinks">
            <img class="socMedia" src="https://www.transparentpng.com/thumb/facebook/facebook-logo-pictures-2.png" alt="facebook logo pictures @transparentpng.com">
            <img class="socMedia" src="https://www.transparentpng.com/thumb/logo-instagram/YfpFOL-logo-instagram-free-transparent.png" alt="Logo Instagram Free Transparent @transparentpng.com">
            <img class="socMedia" src="https://www.transparentpng.com/thumb/linkedin/linkedin-icon-png-4.png" alt="linkedin icon png @transparentpng.com">
            <a target="_blank" href="https://mail.google.com/mail/u/0/?fs=1&to=support@stratguysmarketing.com&tf=cm"><img class="socMedia" src="https://www.transparentpng.com/thumb/email-logo/gmail-logo-png-UbCiBR.png" alt="gmail logo png @transparentpng.com"></a>
        </div>
        <div>
            <span class="footer_link">TRENDING ARTICLES</span>
            <span class="footer_link">TERMS & CONDITIONS</span>
            <span class="footer_link">PRIVACY POLICY</span>
            <span class="footer_link">DISCLAIMER</span>
            <span class="footer_link">CONTACT</span>
        </div>
        <div>
            Copyright 2021 c Strat Guys Marketing
        </div>
    </footer>
</body>
@yield('scripts')
<script>
    function constant (duration, range, current) {
    return duration / range;
    }

    //Linear easing
    function linear (duration, range, current) {
    return ((duration * 5) / Math.pow(range, 2)) * current;
    }

    //Quadratic easing
    function quadratic (duration, range, current) {
    return ((duration * 3) / Math.pow(range, 3)) * Math.pow(current, 2);
    }

    function animateValue(id, start, duration, easing) {
    var end = parseInt(document.getElementById(id).textContent, 10);
    var range = end - start;
    var current = start;
    var increment = end > start? 1 : -1;
    var obj = document.getElementById(id);
    var startTime = new Date();
    var offset = 1;
    var remainderTime = 0;
    
    var step = function() {
        current += increment;
        obj.innerHTML = current.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "+ People on social media";
        
        if (current != end) {
            setTimeout(step, easing(duration, range, current));
        }
        else {
            console.log('Easing: ', easing);
            console.log('Elapsed time: ', new Date() - startTime)
            console.log('');
        }
    };
    
    setTimeout(step, easing(duration, range, start));
    }

    animateValue("socialmedia_accounts", 4659990000, 0, quadratic);

</script>
</html>